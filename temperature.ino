void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  //analogWriteResolution(12);
}

void loop() {
  // put your main code here, to run repeatedly:
  short int value = analogRead(A0);
  short int temp = convert_adc2temperature(value);
  Serial.println(temp);
  delay(1000);
}

short int convert_adc2temperature(short int ax)
{  
   short int tempx;
  // tempx = 10+(ax-815)*(60-10)/(397-815);
   tempx = 10+(ax-597)*(60-10)/(447-577);
   return tempx;
}

