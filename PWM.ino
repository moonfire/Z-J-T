 const int analogOutPin = 3;
 unsigned char ledLight = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  //analogReadResolution(12);

}

void loop() {
  //put your main code here, to run repeatedly:
  short int value = analogRead(A0);
  ledLight = convert_adc2light(value);
  analogWrite( analogOutPin, ledLight);
  Serial.println(ledLight);
  /*
  short int temp = convert_adc2temperature(value);
  Serial.println(temp);
  delay(1000);
  /*
  /*
  ledLight++;
  ledLight %= 256;
  analogWrite( analogOutPin, ledLight);
  */
}

short int convert_adc2temperature(short int ax)
{
   short int tempx;
   tempx = 10+(ax-593)*(60-10)/(335-593);
   return tempx;
}

// convert the readIn value to a range of 0-255
short int convert_adc2light( short int value)
{
  short int result;
  result = (value - 335)*255/(593-335);
  return result;
}

